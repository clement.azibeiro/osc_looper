#ifndef ENGINEMANAGER_H
#define ENGINEMANAGER_H

#ifdef HAVE_CONFIG_H
  #include "config.h"
#else
  #define MAX_ENGINES 10
  #define OSC_PORT 9000
#endif

#include "engine.h"

#include <string>
#include <map>
#include <chrono>

#include <log4cxx/logger.h>

class engine;

class engineManager
{
 public:
  /** \brief Singleton instance accessor
   */
  static engineManager *instance();

  /** \brief Destruct engineManager object
   *
   */
  ~engineManager();

  /** \brief Engines map accessor
   *
   * \return Engines map pointer
   */
  const std::map<std::string, engine *> *get_engines();

  /** \brief Add engine to the manager
   */
  void add(std::string name,
           std::chrono::microseconds period,
           unsigned short int osc_port = OSC_PORT);

  /** \brief List registered engines
   */
  void list();

  /** \brief The logger for the whole class
   *
   * It's using \e liblog4cxx for the logging mechanism.
   */
  static log4cxx::LoggerPtr logger;

 private:
  /** \brief Base constructor
   *
   * This is to construct an engineManager.
   *
   * It is private to implement the singleton design pattern.
   *
   */
  engineManager();

  /** \brief Singleton
   */
  static engineManager *singleton;

  /** \brief List of engines in the whole program
   *
   * Engine dictionary used to keep track of engines defined anywhere
   * in the developper program
   */
  static std::map<std::string, engine *> engines;

  /** \brief OSC control commands
   *
   * These commands are defined with their arguments. \c std::multimap
   * is used to allow similar keys.
   */
  std::multimap<std::string, std::string> control_commands =
  {
    {"/list", ""},
  };

};

#endif // ENGINEMANAGER_H
