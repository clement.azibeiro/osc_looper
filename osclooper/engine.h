#ifndef ENGINE_H
#define ENGINE_H

#ifdef HAVE_CONFIG_H
  #include "config.h"
#else
  #define OSC_PORT 9000
  #define MAX_ENGINES 10
#endif

#include "engineManager.h"
#include "loopManager.h"

#include <chrono>
#include <string>
#include <map>

#include <thread>
#include <mutex>
#include <condition_variable>

#include <log4cxx/logger.h>
#include <log4cxx/basicconfigurator.h>

#include <lo/lo.h>
#include <lo/lo_cpp.h>

class engineManager;

class engine
{
 public:
  /** \brief Base constructor
   *
   * This is to construct an engine with \c noName and \c 1s (one
   * second) as its name and period, respectively.
   *
   */
  engine();

  /** \brief Constructor with name identifier
   *
   * This is to construct an engine with a given name. The name is
   * used to identify the engine uniquely and as the root name in the
   * OSC path to control it.
   *
   * \param name \c std::string Name given to the engine
   */
  explicit engine(std::string name);

  /** \brief Constructor with name and refresh period
   *
   * This is to construct an engine with a given name and a refreshinf
   * period time duration. The name is used to identify the engine
   * inside program and in OSC control path. The period is used to
   * define the time duration consuming between each cycle.
   *
   * \param name \c std::string Name given to the engine
   *
   * \param period \c std::chrono::duration Time duration. Use \c
   * std::literals::chrono_literals to help defining time at several
   * unities ; if not using literals, time is specified in
   * microseconds.
   *
   * \param osc_port \c unsigned \c short \c int OSC server port
   * number, default to port 9000
   */
  explicit engine(std::string name,
                  std::chrono::microseconds period,
                  unsigned short int osc_port = OSC_PORT);

  /** \brief Destruct engine object
   *
   * Stop the running thread. Remove the engine from the manager, then
   * destruct.
   */
  ~engine();

  /** \brief Start the engine's thread
   */
  void start();

  /** \brief Stop the running engine's thread
   */
  void stop();

  /** \brief Name accessor
   *
   * \return Engine's identifier name \c _name as \c std::string
   */
  std::string get_name();

  /** \brief Period time accessor
   *
   * \return Engine's time period \c _period as \c
   * std::chrono::microseconds
   */
  std::chrono::microseconds get_period();

  /** \brief Loop manager accessor
   *
   * \return Loop manager pointer as \c loopManager*
   */
  loopManager *get_loop_manager();

  /** \brief OSC server port accessor
   */
  unsigned short int get_osc_port();

  /** \brief Is engine thread running?
   *
   * \return bool True if the engine is computing the thread loop,
   * false instead
   */
  bool is_running();

  /** \brief The logger for the whole class
   *
   * It's using \e liblog4cxx for the logging mechanism.
   */
  static log4cxx::LoggerPtr logger;

  /** \brief Class scope mutex, helping thread synchronisation
   *
   * The aim of this mutex is to permit locking mechanism within the
   * engine class. One instance can catch the mutex to mmodify the \c
   * std::condition_variable element.
   */
  static std::mutex latch;

  /** \brief Class scope condition_variable
   *
   * This is the \c std::condition_variable controling the threads
   * continuity
   */
  static std::condition_variable condition;

 private:
  /** \brief Loop manager
   *
   * To control loops inside the engine
   */
  loopManager *loopMgr;

  /** \brief Engine name
   *
   * Engine name used as engine's identifier
   */
  std::string _name;

  /** \brief Engine period granularity
   *
   * Engine period granularity, stored in microseconds
   */
  std::chrono::microseconds _period;

  /** \brief Engine thread switch
   *
   * This switch is used to exit the main thread loop. By the way, the
   * engine is not stopped at all, nor destructed. The thread can be
   * restarted with the \c condition_variable mechanism
   */
  bool _running_thread;

  /** \brief Engine thread representation
   */
  std::thread _thread;

  /** \brief OSC server thread
   */
  lo::ServerThread _osc_server;

  /** \brief OSC port number
   */
  unsigned short int _osc_port;

  /** \brief OSC base path
   *
   * The base path is the concatenation of \c / and the engine name
   */
  std::ostringstream _osc_base_path;

  /** \brief OSC control commands
   *
   * These commands are defined with their arguments. \c std::multimap
   * is used to allow similar keys.
   */
  std::multimap<std::string, std::string> control_commands =
  {
    {"/ping", "ss"},
    {"/start", ""},
    {"/start", "T"},
    {"/start", "F"},
    {"/stop", ""},
    {"/stop", "T"},
    {"/stop", "F"},
    {"/new_loop", "si"},
    {"/new_loop", "sii"},
    {"/remove_loop", "s"},
  };

  /** \brief Reference to the global engine manager
   */
  engineManager *_engine_manager;

  /** \brief Initialize OSC for the engine
   *
   * Define which OSC path is allowed inside the engine and map the
   * corresponding callback handler, then start the server.
   */
  void osc_initialization();

  /** \brief Register the current engine object to the engine class
   * container
   */
  void register_engine();

  /** \brief Thread frontend process
   */
  void run();

  /** \brief Thread backend process
   */
  void execute_cycle();

  /** \brief \c liblo OSC handler for the engine method
   *
   * \param path \c char* The path that the incoming message was sent
   * to
   *
   * \param types \c char* If you specided types in your method
   * creation call then this will match those and the incoming types
   * will have been coerced to match, otherwise it will be the types
   * of the arguments of the incoming message
   *
   * \param arguments \c lo_arg** An array of \c lo_arg types
   * containing the values, e.g. if the first argument of the incoming
   * message is of type \c 'f' then the value will be found in \c
   * arguments[0]->f
   *
   * \param nbarguments \c int The number of arguments received
   *
   * \param message \c lo_message A structure containing the original
   * raw message as received. No type coercion will have occured and
   * the data will be in OSC byte order (bigendian)
   *
   * \param user_data \c void* This contains the \c user_data value
   * passed in the call to \c add_method, a \c engine* hopefully
   */
  static int control_handler(const char *path,
                             const char *types,
                             lo_arg **arguments,
                             int nbarguments,
                             lo_message message,
                             void *user_data);

  /** \brief Validation
   *
   * \param returl \c std::string Reference to the provided string URL
   */
  static void validate_returl(std::string &returl);
};

#endif // ENGINE_H
