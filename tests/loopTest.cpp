#include "loopTest.h"
#include "osclooper/loop.h"
#ifdef HAVE_CONFIG_H
  #include "config.h"
#endif

#include <vector>
#include <stdexcept>
#include <chrono>
using namespace std::literals::chrono_literals;

#include <log4cxx/basicconfigurator.h>

// Registers the fixture into the 'registry'
CPPUNIT_TEST_SUITE_REGISTRATION(loopTest);

void loopTest::setUp()
{
  log4cxx::BasicConfigurator::configure();
}

void loopTest::tearDown() {}

void loopTest::testDefaultConstructor()
{
  loop test_loop;
  CPPUNIT_ASSERT("Not implemented yet" == test_loop.get_name());
}
